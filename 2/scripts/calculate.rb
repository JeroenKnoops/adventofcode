#!/usr/bin/env ruby

require_relative "../lib/calculate"

calculate = Calculate.new("input.txt")
calculate.read_file

calculate.calculate_part_1
puts "Result of part 1: #{calculate.total}"
puts "Result of part 2: #{calculate.calculate_part_2}"
