#!/usr/bin/env bats

source frequency.sh

@test "sum values" {
  result="$(sum_values_file ./test.txt)"
  [ "$result" == "2" ]
}

@test "url to file" {
  [ "$filename" == "" ]
  result="$(url_to_file https://gitlab.com/JeroenKnoops/adventofcode/raw/master/README.md )"
  [ "$result" != "" ]
}

@test "concat lines" {
  result="$(concat_lines_file ./test.txt)"
  [ "$result" == "123-4" ]
}
