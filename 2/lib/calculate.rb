class Calculate
  attr_reader :filename
  attr_reader :input
  attr_reader :total

  def initialize(filename = '')
    @filename = filename
  end

  def read_file
    @input = []
    return unless !@filename.empty?
    File.readlines(@filename).each do |line|
      @input << line
    end
  end

  def calculate_part_1(input = @input)
    total = 0
    input.each do |item|
      total += item.to_i
    end
    @total = total
  end

  def calculate_part_2(input = @input)
    total = 0
    frequencies = {}
    frequencies[0] = true

    begin
      input.each do |item|
        total += item.to_i
        return total if frequencies[total]
        frequencies[total]=true
      end
    end until false
  end
end
