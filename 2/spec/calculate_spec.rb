require 'calculate'

RSpec.describe Calculate, "#initialize" do
  context "with a filename" do
    it "sets the filename" do
      filename = "test.txt"
      calculate = Calculate.new(filename)
      expect(calculate.filename).to eq filename
    end
  end
end

RSpec.describe Calculate, "#read_file" do
  context "with a filename" do
    let(:filename) { filename = "./spec/test.txt"}
    let(:calculate) { Calculate.new(filename) }

    it "fills input array" do
      calculate.read_file
      expect(calculate.input.count).to eq 3
    end
  end

  context "without a filename" do
    let(:filename) { ""}
    let(:calculate) { Calculate.new(filename) }

    it "initializes the input array" do
      calculate.read_file
      expect(calculate.input.count).to eq 0
    end
  end
end

RSpec.describe Calculate, "#calculate_part_1" do
  context "positives" do
    let(:input) { ['+1', '+2', '+3', '4'] }
    let(:calculate) { Calculate.new }

    it "sets total" do
      calculate.calculate_part_1 input
      expect(calculate.total).to eq 10
    end
  end

  context "positives and negatives" do
    let(:input) { ['+1', '+2', '-3', '4'] }
    let(:calculate) { Calculate.new }

    it "sets total" do
      calculate.calculate_part_1 input
      expect(calculate.total).to eq 4
    end
  end
end

RSpec.describe Calculate, "#calculate_part_2" do
  context "within single set" do
    let(:input) { ['+1', '+1', '+2', '-3', '4'] }
    let(:calculate) { Calculate.new }

    it "return 1 (1 + 1 + 2 - 3 = 1)" do
      expect(calculate.calculate_part_2 input).to eq 1
    end
  end

  context "needs to run the input set twice" do
    let(:input) { ['+1', '-2', '+3', '+1'] }
    let(:calculate) { Calculate.new }

    it "return 2 (1 -2 + 3 + 1 + 1 - 2 = 4)" do
      expect(calculate.calculate_part_2 input).to eq 2
    end
  end

  context "examples from assignment 1" do
    let(:input) { ['+1', '-1'] }
    let(:calculate) { Calculate.new }

    it "return 0" do
      expect(calculate.calculate_part_2 input).to eq 0
    end
  end

  context "examples from assignment 2" do
    let(:input) { ['+3', '+3', '+4', '-2', '-4'] }
    let(:calculate) { Calculate.new }

    it "return 10" do
      expect(calculate.calculate_part_2 input).to eq 10
    end
  end

  context "examples from assignment 3" do
    let(:input) { ['-6', '+3', '+8', '+5', '-6'] }
    let(:calculate) { Calculate.new }

    it "return 5" do
      expect(calculate.calculate_part_2 input).to eq 5
    end
  end

  context "examples from assignment 4" do
    let(:input) { ['+7', '+7', '-2', '-7', '-4'] }
    let(:calculate) { Calculate.new }

    it "return 14" do
      expect(calculate.calculate_part_2 input).to eq 14
    end
  end
end
