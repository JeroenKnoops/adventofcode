#!/usr/bin/env bash

function url_to_file {
  filename=`mktemp` || exit
  curl $1 -o $filename
  echo "$filename"
}

function sum_values_file {
  echo $(paste -sd+ $1 | bc)
}

function concat_lines_file {
  echo $(cat $1) | sed 's/ //g'
}
